package org.mivoligo.nightclock

import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.widget.TextView
import java.text.SimpleDateFormat
import java.util.*

inline fun SharedPreferences.edit(func: SharedPreferences.Editor.() -> Unit) {
    val editor = edit()
    editor.func()
    editor.apply()
}

fun SharedPreferences.Editor.put(pair: Pair<String, Any>) {
    val key = pair.first
    val value = pair.second
    when (value) {
        is String -> putString(key, value)
        is Int -> putInt(key, value)
        is Boolean -> putBoolean(key, value)
        is Long -> putLong(key, value)
        is Float -> putFloat(key, value)
        else -> error("Only primitive types can be stored in SharedPreferences")
    }
}

operator inline fun <reified T : Any> SharedPreferences.get(key: String, defaultValue: T): T =
        when (T::class) {
            String::class -> getString(key, defaultValue as String) as T
            Int::class -> getInt(key, defaultValue as Int) as T
            Boolean::class -> getBoolean(key, defaultValue as Boolean) as T
            Float::class -> getFloat(key, defaultValue as Float) as T
            Long::class -> getLong(key, defaultValue as Long) as T
            else -> error("Only primitive types can be stored in SharedPreferences")
        }

fun formatTimeAndDate(format: String): String {
    val currentTime = Calendar.getInstance().time
    val sdf = SimpleDateFormat(format)

    return sdf.format(currentTime)
}

// set color for drawables in TextView
fun setTextViewDrawableColor(textView: TextView, color: String) {
    textView.compoundDrawables
            .filterNotNull()
            .forEach { it.colorFilter = PorterDuffColorFilter(Color.parseColor(color), PorterDuff.Mode.SRC_IN) }
}

val dcDigitsList = listOf("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ":")

val dcV1List = listOf(
        R.drawable.dc_v1_0,
        R.drawable.dc_v1_1,
        R.drawable.dc_v1_2,
        R.drawable.dc_v1_3,
        R.drawable.dc_v1_4,
        R.drawable.dc_v1_5,
        R.drawable.dc_v1_6,
        R.drawable.dc_v1_7,
        R.drawable.dc_v1_8,
        R.drawable.dc_v1_9,
        R.drawable.dc_v1_colon
)

val dcV2List = listOf(
       R.drawable.dc_v2_0,
        R.drawable.dc_v2_1,
        R.drawable.dc_v2_2,
        R.drawable.dc_v2_3,
        R.drawable.dc_v2_4,
        R.drawable.dc_v2_5,
        R.drawable.dc_v2_6,
        R.drawable.dc_v2_7,
        R.drawable.dc_v2_8,
        R.drawable.dc_v2_9,
        R.drawable.dc_v2_colon
)

val dcV3List = listOf(
       R.drawable.dc_v3_0,
        R.drawable.dc_v3_1,
        R.drawable.dc_v3_2,
        R.drawable.dc_v3_3,
        R.drawable.dc_v3_4,
        R.drawable.dc_v3_5,
        R.drawable.dc_v3_6,
        R.drawable.dc_v3_7,
        R.drawable.dc_v3_8,
        R.drawable.dc_v3_9,
        R.drawable.dc_v3_colon
)

val dcV4List = listOf(
       R.drawable.dc_v4_0,
        R.drawable.dc_v4_1,
        R.drawable.dc_v4_2,
        R.drawable.dc_v4_3,
        R.drawable.dc_v4_4,
        R.drawable.dc_v4_5,
        R.drawable.dc_v4_6,
        R.drawable.dc_v4_7,
        R.drawable.dc_v4_8,
        R.drawable.dc_v4_9,
        R.drawable.dc_v4_colon
)

val dcV5List = listOf(
       R.drawable.dc_v5_0,
        R.drawable.dc_v5_1,
        R.drawable.dc_v5_2,
        R.drawable.dc_v5_3,
        R.drawable.dc_v5_4,
        R.drawable.dc_v5_5,
        R.drawable.dc_v5_6,
        R.drawable.dc_v5_7,
        R.drawable.dc_v5_8,
        R.drawable.dc_v5_9,
        R.drawable.dc_v5_colon
)

val dcV6List = listOf(
       R.drawable.dc_v6_0,
        R.drawable.dc_v6_1,
        R.drawable.dc_v6_2,
        R.drawable.dc_v6_3,
        R.drawable.dc_v6_4,
        R.drawable.dc_v6_5,
        R.drawable.dc_v6_6,
        R.drawable.dc_v6_7,
        R.drawable.dc_v6_8,
        R.drawable.dc_v6_9,
        R.drawable.dc_v6_colon
)

val dcV7List = listOf(
       R.drawable.dc_v7_0,
        R.drawable.dc_v7_1,
        R.drawable.dc_v7_2,
        R.drawable.dc_v7_3,
        R.drawable.dc_v7_4,
        R.drawable.dc_v7_5,
        R.drawable.dc_v7_6,
        R.drawable.dc_v7_7,
        R.drawable.dc_v7_8,
        R.drawable.dc_v7_9,
        R.drawable.dc_v7_colon
)

fun setDCDigitImage(key: String, dcThemeList: List<Int>): Int {
    val mapped = dcDigitsList.zip(dcThemeList).toMap()

    return mapped[key]!!

}


