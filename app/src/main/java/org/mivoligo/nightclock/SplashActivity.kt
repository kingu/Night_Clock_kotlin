package org.mivoligo.nightclock

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        PreferenceManager.setDefaultValues(this, R.xml.pref_nightclock, false)

        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}
