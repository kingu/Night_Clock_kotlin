package org.mivoligo.nightclock


import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class DCColorsAdapter(val colors: List<DCColors>, val itemClick: (DCColors) -> Unit)
    : RecyclerView.Adapter<DCColorsAdapter.DCColorsViewHolder>() {

    override fun getItemCount() = colors.size

    override fun onBindViewHolder(holder: DCColorsViewHolder?, position: Int) {
        holder?.colorRect?.setBackgroundColor(Color.parseColor(colors[position].color))
        holder?.bind(colors[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DCColorsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.dc_color_fab, parent, false)
        return DCColorsViewHolder(view, itemClick)
    }

    class DCColorsViewHolder(val colorRect: View, val itemClick: (DCColors) -> Unit)
        : RecyclerView.ViewHolder(colorRect) {

        fun bind(item: DCColors) {
            colorRect.setOnClickListener { itemClick(item) }
        }
    }

}