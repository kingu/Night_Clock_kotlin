package org.mivoligo.nightclock

import android.os.Bundle
import com.takisoft.fix.support.v7.preference.PreferenceFragmentCompatDividers


class SettingsFragment : PreferenceFragmentCompatDividers() {
    override fun onCreatePreferencesFix(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.pref_nightclock, rootKey)
    }

}