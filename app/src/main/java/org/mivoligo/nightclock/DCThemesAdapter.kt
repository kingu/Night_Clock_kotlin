package org.mivoligo.nightclock

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.dc_clock_theme_preview.view.*

class DCThemesAdapter(val themes: List<DCThemes>, val itemClick: (DCThemes) -> Unit)
    : RecyclerView.Adapter<DCThemesAdapter.DCThemesViewHolder>(){

    override fun getItemCount() = themes.size

    override fun onBindViewHolder(holder: DCThemesViewHolder?, position: Int) {
        holder?.themePreview?.iv_dc_theme_preview?.setImageResource(themes[position].preview)
        holder?.bind(themes[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DCThemesViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.dc_clock_theme_preview, parent, false)
        return DCThemesViewHolder(view, itemClick)
    }

    class DCThemesViewHolder(val themePreview: View, val itemClick: (DCThemes) -> Unit)
        : RecyclerView.ViewHolder(themePreview) {
        fun bind(item: DCThemes) {
            themePreview.setOnClickListener { itemClick(item) }
        }
    }
}