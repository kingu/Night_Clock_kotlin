package org.mivoligo.nightclock

import android.content.Context
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.OrientationEventListener
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_dctheme_editor.*


class DCThemeEditorFragment : Fragment() {

    lateinit var listener: OnOwnColorSelected

    interface OnOwnColorSelected {
        fun onOwnColorSelected(isBackColor: Boolean)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_dctheme_editor, container, false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is OnOwnColorSelected) {
            listener = context
        } else {
            throw ClassCastException(context.toString() + " must implement OnOwnColorSelected")
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val prefs = PreferenceManager.getDefaultSharedPreferences(activity)
        val isDayMode = prefs[getString(R.string.pref_day_mode_key), false]



        fun setThemePreview(theme: String) {
            if (isDayMode) {
                prefs.edit { put(getString(R.string.pref_day_theme_preview) to theme)}
            } else {
                prefs.edit { put(getString(R.string.pref_night_theme_preview) to theme) }
            }
        }

        fun setForegroundColorPreview(color: String) {
            if (isDayMode) {
                prefs.edit { put(getString(R.string.day_mode_fore_color_preview_key) to color) }
            } else {
                prefs.edit { put(getString(R.string.night_mode_fore_color_preview_key) to color) }
            }
        }

        fun setBackgroundColorPreview(color: String) {
            if (isDayMode) {
                prefs.edit { put(getString(R.string.day_mode_back_color_preview_key) to color) }
            } else {
                prefs.edit { put(getString(R.string.night_mode_back_color_preview_key) to color) }
            }
        }

        rv_clock_theme.setHasFixedSize(true)
        rv_clock_theme.adapter = DCThemesAdapter(getDCThemes(), {
            setThemePreview(it.themeName)
        })

        rv_foreground_color.setHasFixedSize(true)
        rv_foreground_color.adapter = DCColorsAdapter(getDCColors(), {
            setForegroundColorPreview(it.color)
        })

        rv_background_color.setHasFixedSize(true)
        rv_background_color.adapter = DCColorsAdapter(getDCColors(), {
            setBackgroundColorPreview(it.color)
        })

        btn_own_fore_color.setOnClickListener {
            listener.onOwnColorSelected(false)
        }

        btn_own_back_color.setOnClickListener {
            listener.onOwnColorSelected(true)
        }

    }
}
