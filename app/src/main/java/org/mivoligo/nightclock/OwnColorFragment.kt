package org.mivoligo.nightclock


import android.graphics.Color
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import kotlinx.android.synthetic.main.fragment_own_color.*


class OwnColorFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_own_color, container, false)

    }

    override fun onStart() {
        super.onStart()

        val prefs = PreferenceManager.getDefaultSharedPreferences(activity)
        val isDayMode = prefs[getString(R.string.pref_day_mode_key), false]
        val bundleColor = arguments?.getString("selected_color")
        val bundleIsBackColor = arguments?.getBoolean("isBackColor")
        val color = Color.parseColor(bundleColor)
        var red = Color.red(color)
        var green = Color.green(color)
        var blue = Color.blue(color)
        tv_red.text = red.toString()
        tv_green.text = green.toString()
        tv_blue.text = blue.toString()
        sb_red.progress = red
        sb_green.progress = green
        sb_blue.progress = blue

        fun setColor(red: Int, green: Int, blue: Int): String {
            val colorInt =  Color.rgb(red, green, blue)
            return String.format("#%06X", (0xFFFFFF and colorInt))
        }

        fun setForegroundColorPreview(color: String) {
            if (isDayMode) {
                prefs.edit { put(getString(R.string.day_mode_fore_color_preview_key) to color) }
            } else {
                prefs.edit { put(getString(R.string.night_mode_fore_color_preview_key) to color) }
            }
        }

        fun setBackgroundColorPreview(color: String) {
            if (isDayMode) {
                prefs.edit { put(getString(R.string.day_mode_back_color_preview_key) to color) }
            } else {
                prefs.edit { put(getString(R.string.night_mode_back_color_preview_key) to color) }
            }
        }

        sb_red.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar, progress: Int, p2: Boolean) {
                red = progress
                tv_red.text = red.toString()
                if (bundleIsBackColor!!) {
                    setBackgroundColorPreview(setColor(red, green, blue))
                } else {
                    setForegroundColorPreview(setColor(red, green, blue))
                }

            }

            override fun onStartTrackingTouch(p0: SeekBar?) {}

            override fun onStopTrackingTouch(p0: SeekBar?) {}

        })

        sb_green.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar, progress: Int, p2: Boolean) {
                green = progress
                tv_green.text = green.toString()
                if (bundleIsBackColor!!) {
                    setBackgroundColorPreview(setColor(red, green, blue))
                } else {
                    setForegroundColorPreview(setColor(red, green, blue))
                }

            }

            override fun onStartTrackingTouch(p0: SeekBar?) {}

            override fun onStopTrackingTouch(p0: SeekBar?) {}

        })

        sb_blue.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar, progress: Int, p2: Boolean) {
                blue = progress
                tv_blue.text = blue.toString()
                if (bundleIsBackColor!!) {
                    setBackgroundColorPreview(setColor(red, green, blue))
                } else {
                    setForegroundColorPreview(setColor(red, green, blue))
                }

            }

            override fun onStartTrackingTouch(p0: SeekBar?) {}

            override fun onStopTrackingTouch(p0: SeekBar?) {}

        })
    }
}
